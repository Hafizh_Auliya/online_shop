<?php

    class Home extends Controllers{
        public function __construct()
        {
            parent::__construct();
        }

        public function home($params)
        {
            echo "Home Controller";
        }

        public function data($params)
        {
            echo "Data diterima: ".$params;
        }
    

    //METHOD FROM MODEL
    public function cart($params)
    {
        $cart = $this->model->getCart($params);
        echo $cart;
    }

}