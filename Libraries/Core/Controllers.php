<?php

    class Controllers
    {
         public function __construct()
         {
            //INVOKE METHOD LoadModeL() TO BE EXECUTED
            $this->loadModel();
         }
        
         //LOAD MODEL CLASS
        public function loadModel()
        {
            //GET THE NAME OF THE CONTROLLER CLASS AND CONCATENATE WITH WORD＂Model"
            //TO GET MODEL OF CONTROLLER LIKE "homeModeL"
            $model =get_class($this)."Model";
        
            //REFER TO THE MODELS FOLDER
            $routeClass = "Models/".$model.".php";

            //CHECK IF THE MODEL EXISTS
            //IF IT EXISTS CALL THE CLASS ONCE&INSTANCE THE MODEL
            if(file_exists($routeClass))
            {
                require_once($routeClass);
                $this->model=new $model();
            }

        }
    }